describe( 'Home', () => {

  beforeEach(() => {
    cy.visit( 'http://localhost:5173/podcast/788236947' )
  })

  it( 'displays podcast info', () => {
    cy.get( '.podcast-summary' )
      .should( 'exist' )
    cy.get( '.podcast-summary .cover-img' )
      .should('have.attr', 'src')
    cy.get( '.podcast-summary .title-text' )
      .should( 'contain', 'Song Exploder')
    cy.get( '.podcast-summary .author-text' )
      .should( 'contain', 'Hirway')
    cy.get( '.podcast-summary .summary-text' )
      .should( 'exist' )
  })
  Cypress.config('defaultCommandTimeout', 15000);
  it( 'displays episodes', () => {
    cy.get( '.episodes-count' )
      .should( 'exist' )
    cy.get( '.episodes-list' )
      .should( 'exist' )
    cy.get( '.episodes-element' )
      .should( 'exist' )
  })

  it( 'go to details page through cover', () => {
    cy.get( '.cover.link-details' ).click()
  })
  it( 'go to details page through title', () => {
    cy.get( '.title.link-details' ).first().click()
  })
  it( 'go to details page through author', () => {
    cy.get( '.author.link-details' ).first().click()
  })

  it( 'go to episode view', () => {
    cy.get( '.link-episode' ).first().click()
  })
})