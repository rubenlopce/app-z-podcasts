describe( 'Home', () => {

  beforeEach(() => {
    cy.visit( 'http://localhost:5173/' )
  })

  it( 'displays 100 podcast', () => {
    cy.get( '.podcast-list .podcast-item' ).should( 'have.length', 100)
  })
  it( 'go to details page', () => {
    cy.get( '.link-details' ).first().click()
  })
  context( 'search podcast', () => {
    it( 'search by podcast name', () => {
      cy.get( '.filter-bar input' )
      .type( 'The' )

      cy.get( '.podcast-list .podcast-item .title' )
        .should( 'contain', 'The' )      
    })
    it('search by author', () => {
      cy.get( '.filter-bar input' )
        .type( 'The' )
      
      cy.get( '.podcast-list .podcast-item .author' )
        .should( 'contain', 'The' )
    })
  })
})