This is a React project build with Vite

# Getting Started

Install dependencies and run the development server.

```
npm run dev
```

Open http://localhost:5173/ in your browser.

# Build and testing

The testing is done in cypress, before doing it first you need to do a build for production and run it.

```
npm run build
npm run serve
```

After that the App will be running at http://localhost:4173/

Next you should run cypress.

```
npm run cypress
```

In case you want to use cypress on development, you should change the port in the test files.


### Side Notes

* Sometimes could be CORS errors giving how the API works, be mindful of this, reloading a page should do the work.
* In case of the episodes you should go back to the podcast page.
