import Navbar from '@components/UI/Navbar'
import { Link, Outlet, useNavigation } from 'react-router-dom'

export const RootLayout = () => {
  const navigation = useNavigation()
  return <>
          <Navbar>
            <Link to={`/`}><h1>Podcaster</h1></Link>
            { navigation.state !== 'idle' && <div className='pulsating-circle'></div> }
          </Navbar>
          <Outlet />
        </>
}

export default RootLayout