import PodcastEpisodes from "@components/Podcasts/Details/PodcastEpisodes"
import PodcastSummary from "@components/Podcasts/Details/PodcastSummary"
import { useParams } from "react-router-dom"

import './Details.scss'

export const DetailsPage = () => {

  const params = useParams()

  const podcastId = params.podcastId ? params.podcastId : ''

  return <div className='details-body'>
          <PodcastSummary podcastIdProp={podcastId} />
          <PodcastEpisodes podcastIdProp={podcastId} />
        </div>
        
}

export default DetailsPage