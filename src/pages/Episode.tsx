import PodcastSummary from "@components/Podcasts/Details/PodcastSummary"
import PodcastPlayer from "@components/Podcasts/Episode/PodcastPlayer"
import { useParams } from "react-router-dom"

import './Episode.scss'

export const EpisodePage = () => {

  const params = useParams()

  const podcastId = params.podcastId ? params.podcastId : ''
  const episodeId = params.episodeId ? params.episodeId : ''

  return <div className='episode-body'>
          <PodcastSummary podcastIdProp={podcastId} />
          <PodcastPlayer podcastIdProp={podcastId} episodeIdProp={episodeId} />
        </div>
        
}

export default EpisodePage