import PodcastList from "@components/Podcasts/PodcastList"

export const HomePage = () => {

  return <PodcastList />

}

export default HomePage