export interface Episode {
  id: string,
  title: string,
  summary: string,
  date: string,
  duration: string,
  extension: string,
  file: string
}

export default Episode