export interface Podcast {
  id: string,
  title: string,
  image: string,
  author: string,
  summary?: string
}

export default Podcast