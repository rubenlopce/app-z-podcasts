import DetailsPage from "@pages/Details"
import EpisodePage from "@pages/Episode"
import HomePage from "@pages/Home"
import RootLayout from "@pages/Root"
import { checkPodcastList } from "@util/podcast"
import { RouterProvider, createBrowserRouter } from "react-router-dom"

import './App.scss'

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: '/podcast/:podcastId',
        element: <DetailsPage />,
        loader: checkPodcastList
      },
      {
        path: '/podcast/:podcastId/episode/:episodeId',
        element: <EpisodePage />,
        loader: checkPodcastList
      }
    ]
  }
])

const App = () => {

  return <RouterProvider router ={ router } />
}

export default App
