import Episode from "@models/episode.model"
import Podcast from "@models/podcast.model"

export const checkIfPodcastListExist = () => {
  const expiration = getDuration( 'expiration' )
  const data = JSON.parse(localStorage.getItem('podcastList') || '[]')
  if( expiration > 0 && data.length > 0) {
    return true
  }
  return false
}

export const checkIfPodcastDetailsExist = ( id:string, storage: string ) => {
  console.log('Checking if PodcastDetails exist')
  let storageExpiration:string = 'expiration-000000'
  switch (storage) {
    case 'summary':
      storageExpiration = 'expiration-'+id
      break;
    case 'episodes':
      storageExpiration = 'expiration-episodes-'+id
      break;
    default:
      break;
  }
  const expiration = getDuration(storageExpiration)
  const data = JSON.parse( localStorage.getItem( 'podcast-episodes-'+id ) || '[]' )
  if( expiration > 0 && data.length > 0) {
    return true
  }
  return false
}

const getDuration = ( item:string ) => {
  const storedExpirationDate = parseInt( localStorage.getItem( item ) || '0' )
  const now = new Date().getTime()
  const duration = storedExpirationDate - now
  return duration
}

export const checkPodcastList = async () => {
  let data: Podcast[] = []
  if( checkIfPodcastListExist() === false ){
    try {
      await fetch( 'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json' )
            .then( ( response ) => response.json() )
            .then( ( dataResponse ) => {
              data = dataResponse.feed.entry.map( ( element:any ):Podcast => {
                return {
                  'id': element.id.attributes['im:id'],
                  'title': element['im:name'].label,
                  'image': element['im:image'][2].label,
                  'author': element['im:artist'].label,
                  'summary': element.summary.label
                }
              })
            })
    } catch (error) {
      console.log( 'No se ha podido cargar la lista. '+ error)
    }

    let expiration = new Date().getTime()
    expiration = expiration + (86400 * 1000)
    localStorage.setItem('expiration', expiration.toString())
    localStorage.setItem('podcastList', JSON.stringify(data))

  }

  return null

}

export const getEpisode = ( podcastId:string, episodeId:string ) => {
  const storedEpisodes = localStorage.getItem( 'podcast-episodes-'+podcastId )
  const episodes:Episode[] = JSON.parse(storedEpisodes || '')

  return episodes.filter( ( element:Episode ) => element.id == episodeId)[0]
}