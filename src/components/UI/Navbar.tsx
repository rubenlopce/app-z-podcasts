import './Navbar.scss'

const Navbar = ( props: any ) => {
  return <div className='navbar'>{props.children}</div>
}

export default Navbar