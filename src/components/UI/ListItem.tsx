import './ListItem.scss'

const ListItem = (props:any) => {
  const classes = 'list-item ' + props.className
  return <li className={classes}>{props.children}</li>
}

export default ListItem