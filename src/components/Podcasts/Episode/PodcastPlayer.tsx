import Card from "@components/UI/Card"
import Episode from "@models/episode.model"
import { checkIfPodcastDetailsExist, getEpisode } from "@util/podcast"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import parse from "html-react-parser"

import './PodcastPlayer.scss'

const PodcastPlayer = ( props:any ) => {

  const [ episode, setEpisode ] = useState<Episode>()

  const podcastId = props.podcastIdProp
  const episodeId = props.episodeIdProp
  const localStorageExist = checkIfPodcastDetailsExist(props.podcastIdProp, 'episodes')

  useEffect( () => {
    if(localStorageExist) { setEpisode(getEpisode(podcastId, episodeId)) }
  }, [ ] )

  const formatText = ( text:string ) => {
    const formattedText = text.split('\n').map( paragraph => <p>{parse(paragraph)}</p>)
    return formattedText
  }

  const errorMessage = <>
                        <h1>An error ocurred</h1>
                        <Link to={`/podcast/${podcastId}`}>Go back to Podcast page</Link>
                      </>

  let podcastPlayer = <div className='podcast-player_inner'>
                        <h1 className='title'>{episode?.title}</h1>
                        <article>{episode ? formatText(episode.summary) : ''}</article>
                        <audio controls>
                          <source src={episode?.file} type='audio/mpeg' />
                          Your browser does not support the audio element.
                        </audio>
                      </div>

  return <Card className='podcast-player'>
          { !localStorageExist && errorMessage }
          { localStorageExist && podcastPlayer }
        </Card>

}
export default PodcastPlayer