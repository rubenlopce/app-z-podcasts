import Card from "@components/UI/Card"
import Episode from "@models/episode.model"
import { Link } from "react-router-dom"

import './PodcastEpisodesList.scss'

const PodcastEpisodesList = ( props:any ) => {

  const episodeList:Episode[] = props.episodeList
  const podcastId = props.podcastIdProp

  return <Card className='episodes-list_container'>
          <table className='episodes-list'>
            <thead>
              <tr>
                <th>Title</th>
                <th>Date</th>
                <th>Duration</th>
              </tr>
            </thead>
            { 
              episodeList.map( (episode:Episode) => {
                return <tbody className='episodes-element' key={episode.id}>
                        <tr>
                          <td><Link to={`/podcast/${podcastId}/episode/${episode.id}`} className='link-episode'>{episode.title}</Link></td>
                          <td>{episode.date}</td>
                          <td>{episode.duration}</td>
                        </tr>
                      </tbody>
              })
            }
            </table>
        </Card>

}

export default PodcastEpisodesList