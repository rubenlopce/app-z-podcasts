import Card from "@components/UI/Card"
import Podcast from "@models/podcast.model"
import { checkIfPodcastDetailsExist } from "@util/podcast"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"

import './PodcastSummary.scss'

export const PodcastSummary = ( props: any) => {
  const podcastId = props.podcastIdProp

  const [ podcastData, setPodcastData ] = useState<Podcast>()
  const [ isLoading, setIsLoading ] = useState<boolean>(true)

  const fetchPodcastSummary = async () => {
    let data: Podcast = { id: '', title:'', image:'', author: ''}
    const urlLookup = `https://itunes.apple.com/lookup?id=${podcastId}`
    if( checkIfPodcastDetailsExist( podcastId, 'summary' ) === false){
      try {
        await fetch( `https://api.allorigins.win/get?url=${encodeURIComponent(urlLookup)}` )
              .then( ( response ) => response.json() )
              .then( ( dataResponse ) => {
                dataResponse = JSON.parse(dataResponse.contents)
                data = dataResponse.results.map( ( element:any ):Podcast => {
                  return {
                    'id': element.collectionId,
                    'title': element.collectionCensoredName,
                    'image': element.artworkUrl600,
                    'author': element.artistName
                  }
                })[0]
              })
      } catch (error) {
        console.log( 'No se ha podido cargar la información del podcast. '+ error)
      }
      //This api doesn't provide summary, we need to get them through the podcast list in the local storage
      const podcastList = JSON.parse(localStorage.getItem('podcastList') || '[]')
      const currentPodcast = podcastList.filter( ( element:Podcast ) => element.id == podcastId )[0]
      if(currentPodcast){
        data.summary = currentPodcast.summary
      }
      
      let expiration = new Date().getTime()
      expiration = expiration + (86400 * 1000)
      localStorage.setItem('expiration-'+podcastId, expiration.toString())
      localStorage.setItem('podcast-'+podcastId, JSON.stringify(data))
  
    }
    data = JSON.parse(localStorage.getItem('podcast-'+podcastId) || '[]')
    setPodcastData( data )
    setIsLoading( false )
  }

  useEffect( () => {
    fetchPodcastSummary()
  }, [ ])

  const author = <Card className='podcast-summary'>
                  <Link to={`/podcast/${podcastId}`} className='cover link-details'>
                    <img className='cover-img' src={podcastData?.image} alt={podcastData?.title} />
                  </Link>
                  <Link to={`/podcast/${podcastId}`} className='title link-details'>
                    <h1 className='title-text'>
                      {podcastData?.title}
                    </h1>
                  </Link>
                  <Link to={`/podcast/${podcastId}`} className='author link-details'>
                    <h2 className='author-text'>
                      by {podcastData?.author} 
                    </h2>
                  </Link>
                  <p className='summary-text'>
                    <strong>Description:</strong><br /> {podcastData?.summary} 
                  </p>
                </Card>

  return <>{!isLoading && author}</>
        
}

export default PodcastSummary