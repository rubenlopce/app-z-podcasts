import Card from "@components/UI/Card"

import './PodcastEpisodesCount.scss'

const PodcastEpisodesCount = ( props:any ) => {

  return <Card className='episodes-count'>
          <h1>Episodes: {props.episodeCount}</h1>
        </Card>

}

export default PodcastEpisodesCount