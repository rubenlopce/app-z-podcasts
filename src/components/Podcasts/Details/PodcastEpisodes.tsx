import Episode from "@models/episode.model"
import { checkIfPodcastDetailsExist } from "@util/podcast"
import { useEffect, useState } from "react"
import PodcastEpisodesCount from "@components/Podcasts/Details/PodcastEpisodesCount"
import PodcastEpisodesList from "@components/Podcasts/Details/PodcastEpisodesList"

import './PodcastEpisodes.scss'

export const PodcastEpisodes = ( props: any) => {
  const podcastId = props.podcastIdProp

  const [ episodesList, setEpisodesList ] = useState<Episode[]>([])
  const [ isLoading, setIsLoading ] = useState<boolean>(true)

  const fetchPodcastEpisodes = async () => {
    let data: Episode[] = []
    // Limited to 20 for demo purposes
    const urlLookup = `https://itunes.apple.com/lookup?id=${podcastId}&media=podcast&entity=podcastEpisode&limit=20`
    if( checkIfPodcastDetailsExist( podcastId, 'episodes' ) === false){
      try {
        await fetch( `https://api.allorigins.win/get?url=${encodeURIComponent(urlLookup)}` )
              .then( ( response ) => response.json() )
              .then( ( dataResponse ) => {
                dataResponse = JSON.parse(dataResponse.contents)
                data = dataResponse.results
                        .filter( ( element:any ) => element.wrapperType === 'podcastEpisode' )
                        .map( ( element:any ):Episode => {
                          const releaseDate = new Date(element.releaseDate)
                          const simpleDate = `${releaseDate.getDay()}/${releaseDate.getMonth()}/${releaseDate.getFullYear()}`
                          const duration = new Date(element.trackTimeMillis)
                          const durationString = `${(duration.getHours() > 0 ? duration.getHours()+':' : '')}${(duration.getMinutes() < 10 ? '0': '')}${duration.getMinutes()}:${(duration.getSeconds() < 10 ? '0': '')}${duration.getSeconds()}`
                          return {
                            'id': element.trackId,
                            'title': element.trackName,
                            'summary': element.description,
                            'date': simpleDate,
                            'duration': durationString,
                            'extension': element.episodeFileExtension,
                            'file': element.episodeUrl
                          }
                        })
              })
      } catch (error) {
        console.log( 'No se han podido cargar los episodios. '+ error)
      }
      
      let expiration = new Date().getTime()
      expiration = expiration + (86400 * 1000)
      localStorage.setItem('expiration-episodes-'+podcastId, expiration.toString())
      localStorage.setItem('podcast-episodes-'+podcastId, JSON.stringify(data))
  
    }
    data = JSON.parse(localStorage.getItem('podcast-episodes-'+podcastId) || '[]')
    setEpisodesList( data )
    setIsLoading( false )
  }

  useEffect( () => {
    fetchPodcastEpisodes()
  }, [ ])

  const loader = <div className='pulsating-circle' />

  const listOfEpisodes = <>
                          <PodcastEpisodesCount episodeCount={episodesList.length} />
                          <PodcastEpisodesList episodeList={episodesList} podcastIdProp={podcastId} />
                        </>

  return <div className={`details-body_container ${isLoading ? 'loading' : ''}`}>
          {isLoading && loader}
          {!isLoading && listOfEpisodes}
        </div>
        
}

export default PodcastEpisodes