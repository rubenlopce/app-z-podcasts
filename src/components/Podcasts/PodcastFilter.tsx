import './PodcastFilter.scss'

const PodcastFilter = ( props:any ) => {

  const filterChangeHandler = ( event:React.ChangeEvent<HTMLInputElement> ) => {
    props.onChangeFilter(event.target.value)
  }


  return <div className='filter-bar'>
          <p className='count'>{props.podcastCount}</p>
          <input type='text' onChange={filterChangeHandler} />
        </div>
}

export default PodcastFilter