import ListItem from '@components/UI/ListItem'
import Podcast from '@models/podcast.model'
import { useEffect, useState } from 'react'
import PodcastFilter from './PodcastFilter'
import { Link } from 'react-router-dom'
import { checkIfPodcastListExist } from "@util/podcast"

import './PodcastList.scss'

const PodcastList = ( ) => {

  const [ podcastsList, setPodcastsList ] = useState<Podcast[]>([])
  const [ podcastFilter, setPodcastFilter ] = useState<string>('')

  let data: Podcast[] = []

  const fetchPodcastList = async () => {
    console.log(checkIfPodcastListExist())
    if( checkIfPodcastListExist() === false){
      try {
        await fetch( 'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json' )
              .then( ( response ) => response.json() )
              .then( ( dataResponse ) => {
                data = dataResponse.feed.entry.map( (element:any):Podcast => {
                  return {
                    'id': element.id.attributes['im:id'],
                    'title': element['im:name'].label,
                    'image': element['im:image'][2].label,
                    'author': element['im:artist'].label
                  }
                })
              })
      } catch (error) {
        console.log( 'No se ha podido cargar la lista. '+ error)
      }
  
      let expiration = new Date().getTime()
      expiration = expiration + (86400 * 1000)
      localStorage.setItem('expiration', expiration.toString())
      localStorage.setItem('podcastList', JSON.stringify(data))
  
    }
    data = JSON.parse(localStorage.getItem('podcastList') || '[]')
    setPodcastsList( data )
  }


  const filterChangeHandler = ( text:string ) => {
    setPodcastFilter(text.toLowerCase())
  }

  useEffect( () => {
    fetchPodcastList()
  }, [ ])

  const filteredPodcastsList = podcastsList.filter( podcast => {
    return podcast.title.toLowerCase().includes(podcastFilter) || podcast.author.toLowerCase().includes(podcastFilter)
  })

  return <>
          <PodcastFilter onChangeFilter={filterChangeHandler} podcastCount={filteredPodcastsList.length} />
          <ul className='podcast-list'>
            {
              filteredPodcastsList.map( (podcast:Podcast) => {
                return <ListItem className='card podcast-item' key={podcast.id}>
                        <Link to={`/podcast/${podcast.id}`} className='link-details'>
                          <img src={podcast.image} alt={podcast.title} />
                          <h2 className='title' >{podcast.title}</h2>
                          <h3 className='author' >Author: {podcast.author}</h3>
                        </Link>
                      </ListItem>
              })
            }
          </ul>
        </>
}

export default PodcastList